from config import *

from matplotlib import pyplot as plt
import numpy as np

class LayeredSukharevSampler:
    def __init__(self, lower, upper):
        self.lower = lower
        self.upper = upper
        self.sub_sampler1 = None
        self.sub_sampler2 = None
        
    def sample(self):
        if self.sub_sampler1 is None:
            self.sub_sampler1 = LayeredSukharevSampler(self.lower, (self.lower + self.upper) / 2)
            self.sub_sampler2 = LayeredSukharevSampler((self.lower + self.upper) / 2, self.upper)
            return (self.lower + self.upper) / 2
        else:
            current_sampler = self.sub_sampler1
            self.sub_sampler1 = self.sub_sampler2
            self.sub_sampler2 = current_sampler
            return current_sampler.sample()

def rotate(vec, angle):
    rotation_matrix = np.array([
            [np.cos(angle), -np.sin(angle)],
            [np.sin(angle), np.cos(angle)]
        ])
    if vec.shape == (2,):
        return rotation_matrix @ vec
    else:
        return (rotation_matrix @ vec.T).T


def compute_rotation_histogram(scan_vectors, plot=False):
    vecs1 = scan_vectors[:-WALL_ANGLE_STEPSIZE]
    vecs2 = scan_vectors[WALL_ANGLE_STEPSIZE:]

    angles = np.arctan2(vecs2[:,1] - vecs1[:,1], vecs2[:,0] - vecs1[:,0])
    
    hist, bin_edges = np.histogram(angles, bins=ROTATION_HISTOGRAM_RESOLUTION, range=(-np.pi, np.pi))
    
    if plot:
        plt.hist(angles, bins=bin_edges)
        plt.show()

    return hist

def compute_translation_histogram(translations, artificial_std=0, plot=False):
    x_mod = \
        (translations 
        + TRANSLATION_HISTOGRAM_SIZE 
        + np.random.normal(0, artificial_std, len(translations))) % TRANSLATION_HISTOGRAM_SIZE
    
    hist, bin_edges = np.histogram(
        x_mod, 
        bins=TRANSLATION_HISTOGRAM_RESOLUTION, 
        range=(0, TRANSLATION_HISTOGRAM_SIZE)
    )
    
    if plot:
        plt.hist(x_mod, bins=bin_edges)
        plt.show()

    return hist

def delta_ix_to_delta_angle(delta_ix):
    angle = delta_ix * ROTATION_HISTOGRAM_STEPSIZE
    if angle > np.pi:
        angle -= 2 * np.pi
    return angle

def delta_angle_to_delta_ix(angle):
    angle_step_centered = angle + (.5 * ROTATION_HISTOGRAM_STEPSIZE)
    angle_nonnegative = (angle_step_centered + (2 * np.pi)) % (2 * np.pi)
    return int(angle_nonnegative / ROTATION_HISTOGRAM_STEPSIZE)

def delta_ix_to_delta_x(delta_ix):
    delta_x = delta_ix * TRANSLATION_HISTOGRAM_STEPSIZE
    if delta_x > TRANSLATION_HISTOGRAM_SIZE / 2:
        delta_x -= TRANSLATION_HISTOGRAM_SIZE
    return delta_x

def delta_x_to_delta_ix(delta_x):
    x_step_centered = delta_x + (.5 * TRANSLATION_HISTOGRAM_STEPSIZE)
    delta_x_nonnegative = (x_step_centered + TRANSLATION_HISTOGRAM_SIZE) % TRANSLATION_HISTOGRAM_SIZE
    return int(delta_x_nonnegative / TRANSLATION_HISTOGRAM_STEPSIZE)

def rotate_histogram(hist, angle=None, delta_ix=None):
    if delta_ix is None:
        delta_ix = delta_angle_to_delta_ix(angle)

    if delta_ix == 0:
        return np.array(hist)

    hist_rotate = np.zeros(hist.shape)
    
    hist_rotate[delta_ix:] = hist[:-delta_ix]
    hist_rotate[:delta_ix] = hist[-delta_ix:]

    return hist_rotate

def crosscorrelate(s1, s2, mask=None, plot=False):
    assert len(s1) == len(s2)
    ccorr = np.zeros(len(s1)).astype(int)
    for delta_ix in (range(len(ccorr)) if mask is None else np.where(mask)[0]):
        s2_rot = rotate_histogram(s2, delta_ix=delta_ix)
        ccorr[delta_ix] = np.sum(s1 * s2_rot)
    if plot:
        plt.plot(ccorr)
        plt.show()
    return ccorr

def create_sukharev_visualization(num_layers=5):
    num_samples = 0
    for layer in range(num_layers):
        num_samples += num_samples + 1

    # Instanz von LayeredSukharevSampler erstellen
    sampler = LayeredSukharevSampler(0, 2 * np.pi)

    # Kreis zeichnen
    fig, ax = plt.subplots(subplot_kw={'projection': 'polar'})
    ax.set_theta_offset(np.pi/2)
    ax.set_theta_direction(-1)

    # Radien-Zahlen entfernen
    ax.set_yticklabels([])

    # Farben für verschiedene Schichten definieren
    colors = ['blue', 'red', 'green', 'purple', 'orange']

    # Winkel samples und Schichten verfolgen
    angles = [sampler.sample() for _ in range(num_samples)]

    # Punkte auf dem Kreis markieren, Farbe basierend auf der Schicht ändern
    for i, angle in enumerate(angles):
        layer = int(np.log2(i + 1))
        color = colors[layer % len(colors)]
        ax.plot([angle, angle], [0, 1], color=color)

    plt.show()

def create_sukharev_visualizations(num_layers=5, overlay_layers=False):
    num_samples = 0
    for layer in range(num_layers):
        num_samples += num_samples + 1

    sampler = LayeredSukharevSampler(0, 2 * np.pi)
    colors = ['blue', 'red', 'green', 'purple', 'orange']
    angles = [sampler.sample() for _ in range(num_samples)]

    fig, axes = plt.subplots(1, num_layers, subplot_kw={'projection': 'polar'}, figsize=(20, 3))
    plt.subplots_adjust(wspace=0.5)

    current_sample = 0
    for layer in range(num_layers):
        ax = axes[layer]
        ax.set_theta_offset(np.pi/2)
        ax.set_theta_direction(-1)
        ax.set_yticklabels([])
        ax.set_title(f'Schicht {layer + 1}')

        num_samples_in_layer = 2**layer
        start_index = 0 if overlay_layers else current_sample

        for i in range(start_index, current_sample + num_samples_in_layer):
            angle = angles[i]
            line_layer = int(np.log2(i + 1))  # Layer, der die Linie hinzugefügt hat
            color = colors[line_layer % len(colors)]
            ax.plot([angle, angle], [0, 1], color=color)

        current_sample += num_samples_in_layer

    plt.show()
