import navigator
import util
import map
from config import *

import numpy as np
import json
from matplotlib import pyplot as plt

from robosimpy.gui import *
from robosimpy.robots import Robot, Wheel
from robosimpy import worlds

import ti_robots as ti

try:
    import importlib.resources as pkg_resources
except:
    import importlib_resources as pkg_resources

with pkg_resources.open_text(worlds, "empty_world.json" if REAL_WORLD else SIM_WORLD_FILE) as file:
    simple_world = json.load(file)

bot = None
prev_v_left = 0
prev_v_right = 0

robot = Robot(
    state=np.array([5, 5, 0]),
    initial_C_p=np.array([[0., 0, 0],
                        [0, 0., 0],
                        [0, 0, 0.]]),
    wheels={
        "vr": Wheel(+1.0 / 2.0 * np.pi, 0.33/2, 0, 0, steerable=False, motor=True),
        "vl": Wheel(-1.0 / 2.0 * np.pi, 0.33/2, np.pi, 0, steerable=False, motor=True),
    },
    lasers=np.linspace(SENSOR_BOUND_UPPER, SENSOR_BOUND_LOWER, num=SENSOR_RESOLUTION, endpoint=True),
    enclosure=np.array(
        [
            [-0.2, +0.2, +0.2, +0.2],
            [+0.2, +0.2, +0.2, -0.2],
            [+0.2, -0.2, -0.2, -0.2],
            [-0.2, -0.2, -0.2, +0.2],
        ]
    ),
)

robot.pos_odometry = np.array(robot.pos)
robot.theta_odometry = robot.theta

def get_lasers_real(_, api):
    # Get laser data from real robot
    laser_distances = bot.laser.read()[::-1]
    scan_vectors = np.zeros((len(robot.lasers), 2))
    scan_vectors[:,0] = -np.sin(robot.lasers) * laser_distances
    scan_vectors[:,1] = np.cos(robot.lasers) * laser_distances
    hitpoints = util.rotate(scan_vectors, robot.theta - np.pi * 0.5) + robot.pos_odometry
    api.draw_lasers(robot.pos_odometry, hitpoints)
    return laser_distances, scan_vectors

def get_lasers_sim(world, api):
    # Get laser data from simulated robot
    laser_distances, _ = shoot_lasers(
        robot.pos, robot.theta, robot.lasers, world
    )
    laser_distances += np.random.normal(0, SIM_SENSOR_NOISE_STD, len(laser_distances))
    scan_vectors = np.zeros((len(robot.lasers), 2))
    scan_vectors[:,0] = -np.sin(robot.lasers) * laser_distances
    scan_vectors[:,1] = np.cos(robot.lasers) * laser_distances
    hitpoints = util.rotate(scan_vectors, robot.theta - np.pi * 0.5) + robot.pos
    api.draw_lasers(robot.pos, hitpoints)
    return laser_distances, scan_vectors

import matplotlib.pyplot as plt
import numpy as np

def plot_spanned_vectors(scan_vectors, extension_length, skip=1):
    scan_vectors = util.rotate(np.array(scan_vectors)[::skip], 0)

    for i in range(len(scan_vectors)):
        plt.plot([0, scan_vectors[i][0]], [0, scan_vectors[i][1]], c='gray')

    for i in range(len(scan_vectors) - 1):
        direction = (scan_vectors[i+1][0] - scan_vectors[i][0], scan_vectors[i+1][1] - scan_vectors[i][1])
        magnitude = np.sqrt(direction[0]**2 + direction[1]**2)
        direction_normalized = (direction[0]/magnitude, direction[1]/magnitude)
        
        extended_vector_start = (scan_vectors[i][0] - direction_normalized[0] * extension_length, scan_vectors[i][1] - direction_normalized[1] * extension_length)
        extended_vector_end = (scan_vectors[i+1][0] + direction_normalized[0] * extension_length, scan_vectors[i+1][1] + direction_normalized[1] * extension_length)
        
        plt.plot([extended_vector_start[0], extended_vector_end[0]], [extended_vector_start[1], extended_vector_end[1]], 'b-')

    plt.xlabel('X')
    plt.ylabel('Y')
    plt.axis('equal')

    plt.scatter(0, 0, c='red', label='R', zorder=2)
    plt.text(-0.1, 0.1, 'R', fontsize=12, ha='right', c='red', zorder=2)
    plt.show()


def state_update(api: RoboSimPyWidget, world, inputs):
    global control_context, bot, prev_v_left, prev_v_right

    dt = api.dt

    # Error propagation of odometry.
    robot.C_p = update_c_p(
        robot.C_p, robot.theta_odometry, prev_v_left, prev_v_right, dt, robot.wheels["vr"].l, 0.001, 0.001
    )
    api.draw_error_ellipse(robot.C_p, robot.pos_odometry)

    # Update (sim) robot state

    robot.pos[0], robot.pos[1], robot.theta = update_pose(
        robot.pos[0], robot.pos[1], robot.theta, prev_v_left * np.random.normal(1, SIM_WHEEL_NOISE_STD), prev_v_right * np.random.normal(1, SIM_WHEEL_NOISE_STD), dt, robot.wheels["vr"].l
    )

    # Update state estimate by odometry

    robot.pos_odometry[0], robot.pos_odometry[1], robot.theta_odometry = update_pose(
        robot.pos_odometry[0], robot.pos_odometry[1], robot.theta_odometry, prev_v_left, prev_v_right, dt, robot.wheels["vr"].l
    )

    # Update state estimate by map

    laser_distances, scan_vectors = get_lasers_real(world, api) if REAL_WORLD else get_lasers_sim(world, api)

    robot_pose_map = map.update(
        scan_vectors, 
        pos=robot.pos_odometry, 
        theta=robot.theta_odometry, 
        theta_weight=THETA_WEIGHT, 
        dist_threshold_add=DIST_THRESHOLD_ADD, 
        dist_threshold_remove=DIST_THRESHOLD_REMOVE
    )

    robot.pos_odometry[0], robot.pos_odometry[1], robot.theta_odometry = robot_pose_map
    if REAL_WORLD:
        robot.pos[0], robot.pos[1], robot.theta = robot_pose_map

    # Compute navigation

    v_left, v_right = navigator.update(dt, robot_pose_map, inputs, robot.lasers, laser_distances)

    prev_v_left = v_left
    prev_v_right = v_right

    if REAL_WORLD:
        bot.base.set_velocity_lr(v_left, v_right)

    # Visualization

    api.draw_particles(map.get_poses())
    pointcloud_flat = map.get_pointcloud().reshape(-1, map.get_pointcloud().shape[-1])
    pointcloud_subset = pointcloud_flat[np.random.choice(len(pointcloud_flat), 1000)]
    for p in pointcloud_subset:
        api.draw_point(*p, 5)

# Path integration, see Siegwart/Nourbakhsh, p. 188
def update_pose(x, y, theta, vl, vr, dt, l):
    sl = dt * vl
    sr = dt * vr
    b = 2.0 * l
    slsr2 = (sl + sr) / 2.0
    srsl2b = (sr - sl) / (2.0 * b)
    x = x + slsr2 * np.cos(theta + srsl2b)
    y = y + slsr2 * np.sin(theta + srsl2b)
    theta = theta + 2.0 * srsl2b
    return np.array([x, y, theta])

# Motion jacobian, see Siegwart/Nourbakhsh, p. 189
def motion_jacobian(theta, vl, vr, dt, l):
    sl = dt * vl
    sr = dt * vr
    b = 2.0 * l
    ds = (sl + sr) / 2.0
    dtheta = (sr - sl) / b
    tt = theta + dtheta / 2.0
    ss = ds / b
    c = 0.5 * np.cos(tt)
    s = 0.5 * np.sin(tt)
    return np.array(
        [[c - ss * s, c + ss * s], [s + ss * c, s - ss * c], [1.0 / b, -1.0 / b]]
    )

# Pose jacobian, siehe Siegwart/Nourbakhsh, p. 189
def pose_jacobian(theta, vl, vr, dt, l):
    sl = dt * vl
    sr = dt * vr
    b = 2.0 * l
    ds = (sl + sr) / 2.0
    dtheta = (sr - sl) / b
    tt = theta + dtheta / 2.0
    c = np.cos(tt)
    s = np.sin(tt)
    return np.array([[1.0, 0.0, -ds * s], [0.0, 1.0, ds * c], [0.0, 0.0, 1.0]])

# Motion covariance, see siehe Siegwart/Nourbakhsh, p. 188
def motion_covariance(vl, vr, dt, kl, kr):
    sl = dt * vl
    sr = dt * vr
    return np.array([[kr * abs(sr), 0.0], [0.0, kl * abs(sl)]])

# Update of odometry error estimation
def update_c_p(C_p, theta, vl, vr, dt, l, kl, kr):
    f_p = pose_jacobian(theta, vl, vr, dt, l)
    f_delta = motion_jacobian(theta, vl, vr, dt, l)
    c_delta = motion_covariance(vl, vr, dt, kl, kr)
    return f_p @ C_p @ f_p.T + f_delta @ c_delta @ f_delta.T


if __name__ == "__main__":
    app = RoboSimPyApp(
            robot=robot,
            state_update=state_update,
            world=simple_world,
            meter2px=100,
            gui_scale=1.0,
            fixed_timestep=False,
        )
    if REAL_WORLD:
        with ti.robots.PioneerLaserRobot("10.42.0.1", 50051) as bot_:
            bot = bot_
            bot.laser = ti.laser_client.Laser(bot.channel)
            bot.base.set_auto_stop(False)
            app.run()
    else:
        app.run()

    if SAVE_MAP:
        np.savez_compressed('map_' + str(time.time()) + '.npz', pointcloud=map.get_pointcloud(), scan_poses=map.get_poses(), scan_priorities=map.get_scan_priorities(), scan_common_directions=map.get_scan_common_directions())
