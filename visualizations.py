import matplotlib.pyplot as plt
import numpy as np

def plot_wall_angle_viz():
    l_1 = np.array([2, 3])
    l_2 = np.array([-0.5, 2])

    w_12 = l_2 - l_1

    plt.scatter(0, 0, c='red', label='R', zorder=2)
    plt.text(-0.1, 0.1, 'R', fontsize=12, ha='right', c='red', zorder=2)

    plt.arrow(0, 0, l_1[0], l_1[1], head_width=0.2, head_length=0.3, fc='gray', ec='gray', label='$l_1$')
    plt.arrow(0, 0, l_2[0], l_2[1], head_width=0.2, head_length=0.3, fc='gray', ec='gray', label='$l_2$')
    plt.arrow(l_1[0], l_1[1], w_12[0], w_12[1], head_width=0.2, head_length=0.3, fc='blue', ec='blue', label='$w_{12}$')

    plt.text(l_1[0] / 2 - .1, l_1[1] / 2 + .1, '$l_1$', fontsize=12, ha='right', c='gray')
    plt.text(l_2[0] / 2 - .1, l_2[1] / 2 - .1, '$l_2$', fontsize=12, ha='right', c='gray')
    plt.text(l_1[0] + w_12[0] / 2 - .1, l_1[1] + w_12[1] / 2 + .1, '$w_{12}$', fontsize=12, ha='right', c='blue')

    plt.xlabel('X')
    plt.ylabel('Y')
    plt.axis('equal')
    plt.show()

def plot_wall_angle_viz2():
    l_1 = np.array([2, 3])
    l_2 = np.array([-0.5, 2])

    w_12 = l_2 - l_1

    plt.scatter(0, 0, c='red', label='R', zorder=2)
    plt.text(-0.1, 0.1, 'R', fontsize=12, ha='right', c='red', zorder=2)

    # Function to draw an arrow without extending the vector
    def draw_arrow(start, end, label, color):
        direction = (end - start).astype(float)
        length = np.linalg.norm(direction).astype(float)
        direction /= length
        arrow_length = 0.3
        arrow_head = end - arrow_length * direction
        plt.arrow(start[0], start[1], arrow_head[0] - start[0], arrow_head[1] - start[1], head_width=0.2, head_length=0.3, fc=color, ec=color)
        plt.text((start[0] + arrow_head[0]) / 2 - .1, (start[1] + arrow_head[1]) / 2 + .1, label, fontsize=14, ha='right', c=color)

    draw_arrow(np.array([0, 0]), l_1, '$l_1$', 'gray')
    draw_arrow(np.array([0, 0]), l_2, '$l_2$', 'gray')
    draw_arrow(l_1, l_2, '$w_{12}$', 'blue')

    plt.xlabel('X')
    plt.ylabel('Y')
    plt.axis('equal')
    plt.show()

plot_wall_angle_viz2()
