import map
from config import *
import util
import time

import numpy as np

STATE_HALT = -1
STATE_INIT = 0
STATE_FORWARD = 1
STATE_FORWARD_FORCE = 2
STATE_TURN_LEFT = 3
STATE_TURN_RIGHT = 4
STATE_TURN_360 = 5
STATE_TURN_360_INV = 5.5
STATE_TURN_720 = 6
STATE_TURN_ANGLE_AUX1 = 7
STATE_TURN_ANGLE_AUX2 = 8
STATE_TURN_ANGLE_AUX3 = 9
STATE_TURN_ANGLE_INV_AUX3 = 9.5
STATE_TURN_RANDOM = 10
STATE_DRIVE_HOME = 11
STATE_DRIVE_TARGET = 12

emergency = False


control_context = {
    'state': STATE_INIT,
    'v_left': 0,
    'v_left_target': 0,
    'v_right': 0, 
    'v_right_target': 0, 
    'turn_ref_angle': None,
    'turn_random_target_angle': None,
    'home_pose': None,
    'target_pose': None,
    'start_time': None
}

ls_sampler = util.LayeredSukharevSampler(0, 2 * np.pi)

def update(dt, robot_pose, inputs, laser_directions, laser_distances):
    """
    laser_directions: zero ~ front, negative ~ left
    """

    global emergency, control_context

    if 'n' in inputs or None in inputs or emergency:
        emergency = True
        return 0, 0
    
    if control_context['start_time'] is None:
        control_context['start_time'] = time.time()
        return 0, 0
    if time.time() - control_context['start_time'] < 4:
        return 0, 0
    
    if control_context['home_pose'] is None:
        control_context['home_pose'] = np.array(robot_pose)

    scan_vectors = np.zeros((len(laser_directions), 2))
    scan_vectors[:,0] = -np.sin(laser_directions) * laser_distances
    scan_vectors[:,1] = np.cos(laser_directions) * laser_distances

    scan_vectors_front = scan_vectors[np.abs(scan_vectors[:,0]) < DIST_LIMIT_SIDE_NEAR]
    scan_vectors_side = scan_vectors[(np.abs(scan_vectors[:,0]) < DIST_LIMIT_SIDE_FAR) & (np.abs(scan_vectors[:,1]) < DIST_LIMIT_FRONT_NEAR)]

    dist_front_left = scan_vectors_front[scan_vectors_front[:,0] < 0][:,1].min(initial=np.inf)
    dist_front_right = scan_vectors_front[scan_vectors_front[:,0] >= 0][:,1].min(initial=np.inf)

    dist_side_left = np.abs(scan_vectors_side[scan_vectors_side[:,0] < 0][:,0]).min(initial=np.inf)
    dist_side_right = np.abs(scan_vectors_side[scan_vectors_side[:,0] >= 0][:,0]).min(initial=np.inf)

    # State logic
    current_state = control_context['state']
    if current_state == STATE_INIT:
        control_context['state'] = STATE_TURN_360_INV
    elif current_state == STATE_FORWARD:
        if dist_front_right <= DIST_LIMIT_FRONT_NEAR:
            control_context['state'] = STATE_TURN_LEFT if np.random.rand() < .7 else STATE_TURN_RIGHT
        elif dist_front_left <= DIST_LIMIT_FRONT_NEAR:
            control_context['state'] = STATE_TURN_RIGHT if np.random.rand() < .7 else STATE_TURN_LEFT
        elif map.get_current_scan_priority() % 20 == 0:
            control_context['target_pose'] = np.array(robot_pose)
            control_context['state'] = STATE_DRIVE_HOME
    elif current_state == STATE_FORWARD_FORCE:
        if dist_front_right <= DIST_LIMIT_FRONT_NEAR:
            control_context['state'] = STATE_TURN_LEFT if np.random.rand() < .7 else STATE_TURN_RIGHT
        elif dist_front_left <= DIST_LIMIT_FRONT_NEAR:
            control_context['state'] = STATE_TURN_RIGHT if np.random.rand() < .7 else STATE_TURN_LEFT
    elif current_state == STATE_TURN_360:
        if control_context['turn_ref_angle'] is None:
            control_context['turn_ref_angle'] = robot_pose[2]
        relative_angle = (robot_pose[2] - control_context['turn_ref_angle'] + 2 * np.pi) % (2 * np.pi)
        if relative_angle > np.pi:
            control_context['state'] = STATE_TURN_ANGLE_AUX3
    elif current_state == STATE_TURN_360_INV:
        if control_context['turn_ref_angle'] is None:
            control_context['turn_ref_angle'] = robot_pose[2]
        relative_angle = (robot_pose[2] - control_context['turn_ref_angle'] + 2 * np.pi) % (2 * np.pi)
        if 0.2 < relative_angle < np.pi:
            control_context['state'] = STATE_TURN_ANGLE_INV_AUX3
    elif current_state == STATE_TURN_720:
        if control_context['turn_ref_angle'] is None:
            control_context['turn_ref_angle'] = robot_pose[2]
        relative_angle = (robot_pose[2] - control_context['turn_ref_angle'] + 2 * np.pi) % (2 * np.pi)
        if relative_angle > np.pi:
            control_context['state'] = STATE_TURN_ANGLE_AUX1
    elif current_state == STATE_TURN_ANGLE_AUX1:
        relative_angle = (robot_pose[2] - control_context['turn_ref_angle'] + 2 * np.pi) % (2 * np.pi)
        if relative_angle < np.pi:
            control_context['state'] = STATE_TURN_ANGLE_AUX2
    elif current_state == STATE_TURN_ANGLE_AUX2:
        relative_angle = (robot_pose[2] - control_context['turn_ref_angle'] + 2 * np.pi) % (2 * np.pi)
        if relative_angle > np.pi:
            control_context['state'] = STATE_TURN_ANGLE_AUX3
    elif current_state == STATE_TURN_ANGLE_AUX3:
        relative_angle = (robot_pose[2] - control_context['turn_ref_angle'] + 2 * np.pi) % (2 * np.pi)
        if relative_angle < np.pi:
            control_context['turn_ref_angle'] = None
            control_context['state'] = STATE_FORWARD
    elif current_state == STATE_TURN_ANGLE_INV_AUX3:
        relative_angle = (robot_pose[2] - control_context['turn_ref_angle'] + 2 * np.pi) % (2 * np.pi)
        if relative_angle > np.pi:
            control_context['turn_ref_angle'] = None
            control_context['state'] = STATE_TURN_360
    elif current_state == STATE_TURN_RANDOM:
        if control_context['turn_random_target_angle'] is None:
            control_context['turn_random_target_angle'] = ls_sampler.sample()
        relative_angle = (robot_pose[2] - control_context['turn_random_target_angle'] + 2 * np.pi) % (2 * np.pi)
        if relative_angle > np.pi:
            relative_angle -= 2 * np.pi
        if abs(relative_angle) < 3 / 180 * np.pi:
            control_context['turn_random_target_angle'] = None
            control_context['state'] = STATE_FORWARD_FORCE
    elif current_state == STATE_DRIVE_HOME:
        if dist_front_right <= DIST_LIMIT_FRONT_NEAR:
            control_context['state'] = STATE_TURN_LEFT if np.random.rand() < .7 else STATE_TURN_RIGHT
        elif dist_front_left <= DIST_LIMIT_FRONT_NEAR:
            control_context['state'] = STATE_TURN_RIGHT if np.random.rand() < .7 else STATE_TURN_LEFT
        elif np.linalg.norm(control_context['home_pose'][:2] - robot_pose[:2]) < .1:
            if np.random.rand() < .0:
                control_context['state'] = STATE_DRIVE_TARGET
            else:
                control_context['state'] = STATE_TURN_RANDOM
    elif current_state == STATE_DRIVE_TARGET:
        if dist_front_right <= DIST_LIMIT_FRONT_NEAR:
            control_context['state'] = STATE_TURN_LEFT if np.random.rand() < .7 else STATE_TURN_RIGHT
        elif dist_front_left <= DIST_LIMIT_FRONT_NEAR:
            control_context['state'] = STATE_TURN_RIGHT if np.random.rand() < .7 else STATE_TURN_LEFT
        elif np.linalg.norm(control_context['target_pose'][:2] - robot_pose[:2]) < .1:
            control_context['state'] = STATE_FORWARD_FORCE
    elif current_state in [STATE_TURN_LEFT, STATE_TURN_RIGHT]:
        if np.min(scan_vectors_front[:,1]) > DIST_LIMIT_FRONT_FAR:
            control_context['state'] = STATE_FORWARD
    else:
        control_context['state'] = STATE_HALT

    #print(control_context['state'], map.get_current_scan_priority())
    
    # Motor commands according to states
    current_state = control_context['state']
    if current_state in [STATE_FORWARD, STATE_FORWARD_FORCE]:
        control_context['v_right_target'] = V_FORWARD if dist_side_left > DIST_LIMIT_SIDE_FAR else .8 * V_FORWARD
        control_context['v_left_target'] = V_FORWARD if dist_side_right > DIST_LIMIT_SIDE_FAR else .8 * V_FORWARD
    elif current_state == STATE_TURN_LEFT:
        control_context['v_right_target'] = V_TURN
        control_context['v_left_target'] = -V_TURN
    elif current_state == STATE_TURN_RIGHT:
        control_context['v_right_target'] = -V_TURN
        control_context['v_left_target'] = V_TURN
    elif current_state in [STATE_TURN_360, STATE_TURN_720, STATE_TURN_ANGLE_AUX1, STATE_TURN_ANGLE_AUX2, STATE_TURN_ANGLE_AUX3]:
        control_context['v_right_target'] = V_TURN
        control_context['v_left_target'] = -V_TURN
    elif current_state in [STATE_TURN_360_INV, STATE_TURN_ANGLE_INV_AUX3]:
        control_context['v_right_target'] = -V_TURN
        control_context['v_left_target'] = V_TURN
    elif current_state == STATE_TURN_RANDOM:
        if control_context['turn_random_target_angle'] is not None:
            relative_angle = (control_context['turn_random_target_angle'] - robot_pose[2] + 2 * np.pi) % (2 * np.pi)
            if relative_angle > np.pi:
                relative_angle -= 2 * np.pi
            if relative_angle < 0:
                control_context['v_right_target'] = -V_TURN
                control_context['v_left_target'] = V_TURN
            if relative_angle > 0:
                control_context['v_right_target'] = V_TURN
                control_context['v_left_target'] = -V_TURN
        else:
            control_context['v_right_target'] = 0
            control_context['v_left_target'] = 0
    elif current_state == STATE_DRIVE_HOME:
        home_pos = control_context['home_pose'][:2]
        target_angle = np.arctan2(home_pos[1] - robot_pose[1], home_pos[0] - robot_pose[0])
        relative_angle = (target_angle - robot_pose[2] + 2 * np.pi) % (2 * np.pi)
        if relative_angle > np.pi:
            relative_angle -= 2 * np.pi
        if relative_angle < -20 / 180 * np.pi:
            control_context['v_right_target'] = -V_TURN
            control_context['v_left_target'] = V_TURN
        elif relative_angle > 20 / 180 * np.pi:
            control_context['v_right_target'] = V_TURN
            control_context['v_left_target'] = -V_TURN
        else:
            control_context['v_right_target'] = V_FORWARD if relative_angle > 3 / 180 * np.pi else .8 * V_FORWARD
            control_context['v_left_target'] = V_FORWARD if relative_angle < -3 / 180 * np.pi else .8 * V_FORWARD
    elif current_state == STATE_DRIVE_TARGET:
        target_pos = control_context['target_pose'][:2]
        target_angle = np.arctan2(target_pos[1] - robot_pose[1], target_pos[0] - robot_pose[0])
        relative_angle = (target_angle - robot_pose[2] + 2 * np.pi) % (2 * np.pi)
        if relative_angle > np.pi:
            relative_angle -= 2 * np.pi
        if relative_angle < -20 / 180 * np.pi:
            control_context['v_right_target'] = -V_TURN
            control_context['v_left_target'] = V_TURN
        elif relative_angle > 20 / 180 * np.pi:
            control_context['v_right_target'] = V_TURN
            control_context['v_left_target'] = -V_TURN
        else:
            control_context['v_right_target'] = V_FORWARD if relative_angle > 3 / 180 * np.pi else .8 * V_FORWARD
            control_context['v_left_target'] = V_FORWARD if relative_angle < -3 / 180 * np.pi else .8 * V_FORWARD
    else:
        control_context['v_right_target'] = 0
        control_context['v_left_target'] = 0



    delta_v_acc = dt * MAX_ACC
    delta_v_brk = dt * MAX_BRK

    v_right_target = control_context['v_right_target']
    v_right = control_context['v_right']
   
    if v_right <= v_right_target:
        if v_right_target - v_right < delta_v_acc:
            v_right = v_right_target
        else:
            v_right += delta_v_acc
    else:
        if v_right - v_right_target < delta_v_brk:
            v_right = v_right_target
        else:
            v_right -= delta_v_brk

    control_context['v_right'] = v_right

    
    v_left_target = control_context['v_left_target']
    v_left = control_context['v_left']
   
    if v_left <= v_left_target:
        if v_left_target - v_left < delta_v_acc:
            v_left = v_left_target
        else:
            v_left += delta_v_acc
    else:
        if v_left - v_left_target < delta_v_brk:
            v_left = v_left_target
        else:
            v_left -= delta_v_brk

    control_context['v_left'] = v_left

    if not AUTOPILOT:
        v_right = (V_FORWARD if "e" in inputs else -V_FORWARD if "d" in inputs else 0.0)
        v_left = (V_FORWARD if "q" in inputs else -V_FORWARD if "a" in inputs else 0.0)

    return v_left, v_right