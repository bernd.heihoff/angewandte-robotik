import numpy as np

####################
### BEGIN CONFIG ###
####################

# Autonomous Control
AUTOPILOT = True  # Enable autonomous mode, otherwise control using A, Q, D, E

# Environment Configuration
REAL_WORLD = False                      # Toggle between real-world and simulated environment
SIM_WORLD_FILE = 'island_world.json'    # Choose world file in case of simulation
# 'island_world_diagonal.json'
# 'simple_world.json'
# 'simplest_world.json'
SAVE_MAP = False                        # Save the generated map to a file

# Obstacle Detection
DIST_LIMIT_FRONT_NEAR = 0.6  # Front near distance limit for obstacle detection
DIST_LIMIT_FRONT_FAR = 0.8   # Front far distance limit for obstacle detection
DIST_LIMIT_SIDE_NEAR = 0.3   # Side near distance limit for obstacle detection
DIST_LIMIT_SIDE_FAR = 0.4    # Side far distance limit for obstacle detection

# Distance Thresholds for Map Generation
DIST_THRESHOLD_ADD = .5      # Threshold pose distance for adding scans (combines Euclidean and angle distance)
DIST_THRESHOLD_REMOVE = .5   # Threshold pose distance for removing scans (combines Euclidean and angle distance)
THETA_WEIGHT = 2             # Weight for theta in pose distance: weight=1 -> maximum distance by theta alone is 1

# Velocity Configuration
V_FORWARD = 0.15  # Forward velocity
V_TURN = 0.04     # Turn velocity
MAX_ACC = 0.3     # Maximum acceleration
MAX_BRK = 0.5     # Maximum brake

# Simulation Noise
SIM_WHEEL_NOISE_STD = 0.03        # Standard deviation for simulating wheel noise
SIM_SENSOR_NOISE_STD = 0.005      # Standard deviation for simulating sensor noise

# Sensor Configuration
SENSOR_RESOLUTION = 361           # Sensor resolution
SENSOR_BOUND_LOWER = -np.pi / 2   # Lower bound for sensor
SENSOR_BOUND_UPPER = np.pi / 2    # Upper bound for sensor

# Wall Detection
WALL_ANGLE_STEPSIZE = 10  # Stepsize for wall angle detection (triangulating neighboring laser hitpoints)

# Histogram Configuration
ROTATION_HISTOGRAM_RESOLUTION = 360             # Number of bins for rotation histogram
TRANSLATION_HISTOGRAM_SIZE = 30                 # Value range of translation histogram (double room size)
TRANSLATION_HISTOGRAM_RESOLUTION = 3000         # Number of bins for translation histogram

# Search Deviation
ANGLE_SEARCH_MAX_DEVIATION = 20 * np.pi / 180   # Maximum deviation for angle search in localization
TRANSLATION_SEARCH_MAX_DEVIATION = 0.1          # Maximum deviation for translation search in localization

##################
### END CONFIG ###
##################

# Derived Constants
ROTATION_HISTOGRAM_STEPSIZE = 2 * np.pi / ROTATION_HISTOGRAM_RESOLUTION
TRANSLATION_HISTOGRAM_STEPSIZE = TRANSLATION_HISTOGRAM_SIZE / TRANSLATION_HISTOGRAM_RESOLUTION
