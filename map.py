import util
from config import *

import numpy as np
from matplotlib import pyplot as plt

map_context = {
    'pose_estimate': np.zeros(3),
    'scan_poses': np.zeros((0,3)), 
    'rot_hists': np.zeros((0,ROTATION_HISTOGRAM_RESOLUTION)), 
    'trans_common_directions': np.zeros((0,)),
    'trans_x_hists': np.zeros((0,TRANSLATION_HISTOGRAM_RESOLUTION)), 
    'trans_y_hists': np.zeros((0,TRANSLATION_HISTOGRAM_RESOLUTION)), 
    'priorities': [],
    'pointcloud': np.zeros((0,SENSOR_RESOLUTION,2)),
    'current_priority': 0
}

def add_scan(pose, scan_vectors, priority, plot=False):
    global map_context

    assert len(pose) == 3

    rot_hist = util.compute_rotation_histogram(scan_vectors)
    adjusted_rot_hist = util.rotate_histogram(rot_hist, pose[2])

    max_wall_direction = util.delta_ix_to_delta_angle(np.argmax(adjusted_rot_hist)) - np.pi / 2

    trans_common_direction = (max_wall_direction - np.pi / 2 + 2 * np.pi) % (2 * np.pi)

    x_rot_angle = -trans_common_direction - np.pi / 2
    common_adjusted_scan_vectors = util.rotate(scan_vectors, pose[2] + x_rot_angle)

    adjusted_trans_x_hist = util.compute_translation_histogram(common_adjusted_scan_vectors[:,0])
    adjusted_trans_y_hist = util.compute_translation_histogram(common_adjusted_scan_vectors[:,1])

    map_context['scan_poses'] = np.concatenate([map_context['scan_poses'], [pose]])
    map_context['rot_hists'] = np.concatenate([map_context['rot_hists'], [adjusted_rot_hist]])
    map_context['trans_common_directions'] = np.concatenate([map_context['trans_common_directions'], [trans_common_direction]])
    map_context['trans_x_hists'] = np.concatenate([map_context['trans_x_hists'], [adjusted_trans_x_hist]])
    map_context['trans_y_hists'] = np.concatenate([map_context['trans_y_hists'], [adjusted_trans_y_hist]])
    map_context['priorities'] = np.concatenate([map_context['priorities'], [priority]])

    # TODO replace with util.rotate
    rot_matrix = np.array([
        [np.cos(pose[2]), -np.sin(pose[2])],
        [np.sin(pose[2]), np.cos(pose[2])]
    ])
    transformed_pointcloud = (rot_matrix @ (scan_vectors[:,::-1] * [1,-1]).T).T + pose[:2]
    map_context['pointcloud'] = np.concatenate([map_context['pointcloud'], [transformed_pointcloud]])

def set_pose_estimate(pose):
    map_context['pose_estimate'] = np.array(pose)

def get_pose_estimate():
    return map_context['pose_estimate']

def get_scans():
    return zip(np.array(map_context['scan_poses']), np.array(map_context['rot_hists']))

def get_poses():
    return np.array(map_context['scan_poses'])

def get_pointcloud():
    return np.array(map_context['pointcloud'])

def get_closest_scan(pose, theta_weight=1):
    """
    Note: theta distance is between 0 and 1, choose weighting accordingly.
    """

    if len(map_context['rot_hists']) == 0:
        return -1, -1

    dists = get_scans_dist(pose, theta_weight=theta_weight)

    return np.argmin(dists), np.min(dists)

def get_scans_dist(pose, theta_weight=1):
    """
    Note: theta distance is between 0 and 1, choose weighting accordingly.
    """

    xy_dists = np.sqrt(np.sum((pose[:2] - map_context['scan_poses'][:,:2]) ** 2, axis=1))

    scan_theta_unit_vectors = np.hstack([np.cos(map_context['scan_poses'][:,2])[:,None], np.sin(map_context['scan_poses'][:,2])[:,None]])
    query_theta_unit_vector = np.array([np.cos(pose[2]), np.sin(pose[2])])
    theta_dists = np.abs(np.arccos(np.clip(scan_theta_unit_vectors @ query_theta_unit_vector, -1, 1)) / np.pi)

    dists = xy_dists + theta_weight * theta_dists

    return dists

def get_current_scan_priority():
    return map_context['current_priority']

def get_scan_priorities():
    return map_context['priorities']

def get_scan_common_directions():
    return map_context['trans_common_directions']

def update_theta(prev_pose, scan_vectors, closest_scan_ix):
    global map_context

    prev_theta = prev_pose[2]

    delta_ix_estimate = util.delta_angle_to_delta_ix(prev_theta)
    closest_rot_hist = map_context['rot_hists'][closest_scan_ix]

    curr_rot_hist = util.compute_rotation_histogram(scan_vectors)

    ccorr_ixs = np.arange(ROTATION_HISTOGRAM_RESOLUTION)
    ccorr_search_deviations = \
        np.minimum(
            np.abs(ccorr_ixs - delta_ix_estimate), 
            np.minimum(
                np.abs(ccorr_ixs - (delta_ix_estimate + len(ccorr_ixs))), 
                np.abs(ccorr_ixs - (delta_ix_estimate - len(ccorr_ixs)))
            )
        )
    angle_search_window = ccorr_search_deviations < util.delta_angle_to_delta_ix(ANGLE_SEARCH_MAX_DEVIATION)
    ccorr = util.crosscorrelate(closest_rot_hist, curr_rot_hist, mask=angle_search_window)

    ccorr *= angle_search_window.astype(int)

    curr_theta = util.delta_ix_to_delta_angle(np.argmax(ccorr))
    if curr_theta > np.pi:
        curr_theta -= 2 * np.pi

    return curr_theta

def update_pos(prev_pose, scan_vectors, closest_scan_ix, plot=False):
    global map_context

    common_direction = map_context['trans_common_directions'][closest_scan_ix]
    closest_x_hist = map_context['trans_x_hists'][closest_scan_ix]
    closest_y_hist = map_context['trans_y_hists'][closest_scan_ix]
    closest_scan_pos = map_context['scan_poses'][closest_scan_ix]

    # Rotate position vectors to match most common direction

    prev_pos = prev_pose[:2]
    prev_pos_rotated = util.rotate(prev_pos, -common_direction)

    closest_scan_pos = map_context['scan_poses'][closest_scan_ix][:2]
    closest_scan_pos_rotated = util.rotate(closest_scan_pos, -common_direction)

    delta_ix_x_estimate = util.delta_x_to_delta_ix(prev_pos_rotated[0] - closest_scan_pos_rotated[0])
    delta_ix_y_estimate = util.delta_x_to_delta_ix(prev_pos_rotated[1] - closest_scan_pos_rotated[1])

    # Rotate scan vectors to match most common direction

    rotated_scan_vectors = util.rotate(scan_vectors, -np.pi / 2 + prev_pose[2] - common_direction)

    # Compute maximum of x cross-correlation

    curr_x_hist = util.compute_translation_histogram(rotated_scan_vectors[:,0], artificial_std=.02)
    ccorr_x_ixs = np.arange(len(curr_x_hist))
    ccorr_x_search_deviations = \
        np.minimum(
            np.abs(ccorr_x_ixs - delta_ix_x_estimate), 
            np.minimum(
                np.abs(ccorr_x_ixs - (delta_ix_x_estimate + len(ccorr_x_ixs))), 
                np.abs(ccorr_x_ixs - (delta_ix_x_estimate - len(ccorr_x_ixs)))
            )
        )
    trans_x_search_window = ccorr_x_search_deviations < util.delta_x_to_delta_ix(TRANSLATION_SEARCH_MAX_DEVIATION)
    ccorr_x = util.crosscorrelate(closest_x_hist, curr_x_hist, mask=trans_x_search_window)
    ccorr_x *= trans_x_search_window.astype(int)
    curr_x = util.delta_ix_to_delta_x(np.argmax(ccorr_x))

    # Compute maximum of y cross-correlation

    curr_y_hist = util.compute_translation_histogram(rotated_scan_vectors[:,1], artificial_std=.01)
    ccorr_y_ixs = np.arange(len(curr_y_hist))
    ccorr_y_search_deviations = \
        np.minimum(
            np.abs(ccorr_y_ixs - delta_ix_y_estimate), 
            np.minimum(
                np.abs(ccorr_y_ixs - (delta_ix_y_estimate + len(ccorr_y_ixs))), 
                np.abs(ccorr_y_ixs - (delta_ix_y_estimate - len(ccorr_y_ixs)))
            )
        )
    trans_y_search_window = ccorr_y_search_deviations < util.delta_x_to_delta_ix(TRANSLATION_SEARCH_MAX_DEVIATION)
    ccorr_y = util.crosscorrelate(closest_y_hist, curr_y_hist, mask=trans_y_search_window)
    ccorr_y *= trans_y_search_window.astype(int)
    curr_y = util.delta_ix_to_delta_x(np.argmax(ccorr_y))

    return closest_scan_pos + util.rotate(np.array([curr_x, curr_y]), common_direction)

def remove_invalid_scans(odom_pose_estimate, theta_weight, dist_threshold_remove):
    # Remove scans that (a) are within threshold radius and (b) have lower priority than current estimate

    scan_dists = get_scans_dist(odom_pose_estimate, theta_weight=theta_weight)

    scans_neighborhood = np.where(scan_dists <= dist_threshold_remove)[0]

    if len(scans_neighborhood) > 0:
        priorities_neighborhood = np.array(map_context['priorities'])[scans_neighborhood]

        reference_scan_priority = np.min(priorities_neighborhood)

        invalid_scans_neighborhood = scans_neighborhood[priorities_neighborhood > reference_scan_priority + 1]

        map_context['scan_poses'] = np.delete(map_context['scan_poses'], invalid_scans_neighborhood, axis=0)
        map_context['rot_hists'] = np.delete(map_context['rot_hists'], invalid_scans_neighborhood, axis=0)
        map_context['trans_common_directions'] = np.delete(map_context['trans_common_directions'], invalid_scans_neighborhood, axis=0)
        map_context['trans_x_hists'] = np.delete(map_context['trans_x_hists'], invalid_scans_neighborhood, axis=0)
        map_context['trans_y_hists'] = np.delete(map_context['trans_y_hists'], invalid_scans_neighborhood, axis=0)
        map_context['priorities'] = np.delete(map_context['priorities'], invalid_scans_neighborhood, axis=0)


def update(scan_vectors, theta_weight=1, dist_threshold_add=0.5, dist_threshold_remove=0.5, pos=None, theta=None, plot=False):

    prev_pose_estimate = map_context['pose_estimate']
    odom_pose_estimate = np.array(prev_pose_estimate)
    
    # Update odometry

    if pos is not None:
        odom_pose_estimate[:2] = pos

    if theta is not None:
        odom_pose_estimate[2] = theta

    curr_pose_estimate = np.array(odom_pose_estimate)

    # Remove invalid scans

    remove_invalid_scans(odom_pose_estimate, theta_weight, dist_threshold_remove)

    # Update pose from reference scan

    closest_scan_ix, closest_scan_dist  = get_closest_scan(prev_pose_estimate, theta_weight=theta_weight)
    if closest_scan_ix == -1:
        add_scan(odom_pose_estimate, scan_vectors, map_context['current_priority'])
        return np.array(odom_pose_estimate)

    closest_scan_priority = map_context['priorities'][closest_scan_ix]
    
    curr_pose_estimate[2] = update_theta(odom_pose_estimate, scan_vectors, closest_scan_ix)
    curr_pose_estimate[:2] = update_pos(curr_pose_estimate, scan_vectors, closest_scan_ix)

    # Update map and add new scan

    map_context['pose_estimate'] = np.array(curr_pose_estimate)
    map_context['current_priority'] = closest_scan_priority + 1
    if closest_scan_dist > dist_threshold_add:
        add_scan(curr_pose_estimate, scan_vectors, map_context['current_priority'])

    return curr_pose_estimate
