# Konfiguration

Für verschiedene Einstellungen des Verfahrens und des Programmablaufs siehe config.py. 
Hier wird auch zwischen realem und simuliertem Roboter gewechselt. 

# Ausführung

Nach der Konfiguration wird das Hauptprogramm mit slam.py gestartet.

