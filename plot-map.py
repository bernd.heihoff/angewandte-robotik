#!/bin/python3

import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt
import os
import subprocess
import json

maps = sorted(list(filter(lambda fn: 'map_' in fn and '.npz' in fn, os.listdir())))

for ix, m in enumerate(maps):
    result = subprocess.check_output(
        ['git', 'log', '-1', '--', m],
        stderr=subprocess.STDOUT,
        text=True    
    )
    try:
        lines = result.split('\n')
        commit_hash = lines[0].split(' ')[1]
        commit_message = lines[-2]
        while len(commit_message) and commit_message[0] == ' ':
            commit_message = commit_message[1:]
    except:
        commit_hash = 'No commit hash'
        commit_message = 'No commit message'
    print(f"{ix}: [{commit_hash[:6]} -- {m}] {commit_message}")
    
while 1:
    try:
        ix = int(input('Map index: '))
        if ix == -1:
            map_choice = None
        else:
            map_choice = maps[ix]
        break
    except:
        pass

data = np.load(map_choice) if map_choice is not None else None

plot_walls = input('Do you want to plot a world? (yes/no): [no] ').strip().lower()
if plot_walls == 'yes':
    wall_directory = 'robosimpy/worlds'
    
    files = os.listdir(wall_directory)
    for ix, file_name in enumerate(files):
        print(f"{ix}: {file_name}")

    while True:
        try:
            file_index = int(input('Enter the index of the world file: '))
            wall_file_path = os.path.join(wall_directory, files[file_index])
            break
        except:
            print("Invalid index. Please try again.")

    try:
        with open(wall_file_path, 'r') as file:
            wall_data = json.load(file)
            for segment in wall_data:
                x_values = [segment[0], segment[2]]
                y_values = [segment[1], segment[3]]
                plt.plot(x_values, y_values, c='green') 
    except Exception as e:
        print(f"An error occurred while reading the world file: {e}")


plt.gca().set_aspect('equal')

if data is not None:
    plt.scatter(*data['pointcloud'].reshape(-1, 2).T, alpha=1, c='blue', s=0.5)

    plot_poses = input('Do you want to plot the poses? (yes/no): [yes] ').strip().lower()
    if plot_poses != 'no':
        for i in range(len(data['scan_poses'])):
            t = mpl.markers.MarkerStyle(marker='$\\longrightarrow$').scaled(1.2)
            t._transform = t.get_transform().translate(0.5, -0.05)
            t._transform = t.get_transform().rotate(data['scan_poses'][i, 2])
            plt.scatter(*data['scan_poses'][i, :2], c='red', marker=t)
            plt.scatter(*data['scan_poses'][i, :2], c='black', s=10)


plt.show()

