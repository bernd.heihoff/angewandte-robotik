import ti_robots as ti
import time 

with ti.robots.PioneerLaserRobot("10.42.0.1", 50051) as bot:

    bot.laser = ti.laser_client.Laser(bot.channel)
    
    
    
    
    
    #bot.base.set_auto_stop(True)

    #bot.base.move(0.1)

    # get and print 10 laser readings:
    for x in range(10):
        print("reading number " + str(x) + " : ")
        # read laser scan xy (returns list of x and y values) - only available with tim561
        #a = bot.laser.readXY()
        # read laser scan with distance data 
        a = bot.laser.read()
        print(a)
        print("number of values: " + str(len(a)))
        time.sleep(2)

